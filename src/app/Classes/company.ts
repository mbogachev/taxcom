export class Company{
    id: number;
    name: string;
    INN: string;
    KPP: string;
}