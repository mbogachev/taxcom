export class ESignature{
    'name':string;
    'description':string;
    'price':number;
    'fast_price':number;
    'link':number;
    'fast_link':number;
}