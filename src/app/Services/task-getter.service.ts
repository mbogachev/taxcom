import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { task } from '../Classes/tasks';
import { build } from '../Classes/build';

@Injectable({
    providedIn: 'root'
})

export class TasksGetter{
    getTask(){
        return this.http.get<task>(build.buildURI+'/assets/tasks/task.json');
    }
    setTask(){
        let date = new Date();
        let task = { id: 0, name:'Задача запроса на создание ЭЦП TaxCom', dateTime: date.getFullYear()+'-'+date.getMonth()+
        '-'+date.getDay()+' '+date.getHours()+'-'+date.getMinutes()+'-'+date.getSeconds()}
        let formData = new FormData();
        formData.append('task', JSON.stringify(task))
        return this.http.post(build.buildAPI, formData);
    }
    constructor(private http: HttpClient) { }
}