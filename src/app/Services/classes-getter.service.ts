import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Classes } from '../Classes/classes';
import {build} from '../Classes/build';

@Injectable({
  providedIn: 'root'
})
export class ClassesGetterService {
  GetClasses(){
    return this.http.get<Classes>(build.buildURI+'/assets/links.json');
  }

  constructor(private http:HttpClient) { }
}
