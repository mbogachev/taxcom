import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { build } from '../Classes/build';

@Injectable({
    providedIn: 'root'
})

export class FiltersGetterService{
    getAFilter(){
        try{
            return this.http.get(build.buildURI + '/assets/cities/altayskiy_kray/filters_ac.json');
        }catch(error){console.log(error); return null;}
    }
    getESFilter(){
        try { return this.http.get(build.buildURI +'/assets/cities/altayskiy_kray/filters_es.json');}
        catch (error) { console.log(error); return null; }
    }

    constructor(private http: HttpClient) {}
}