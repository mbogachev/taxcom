import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { ESignature } from '../Classes/electronic_signatures';
import { build } from '../Classes/build';

@Injectable({
  providedIn: 'root'
})

export class ProductGetterService {
  getElectronicSignatures(){
    return this.http.get<ESignature>(build.buildURI+'/assets/cities/altayskiy_kray/electronic_signatures.json');
  }
  getAccounting() {
    let products;
    try {
      this.http.get(build.buildURI + '/assets/cities/altayskiy_kray/accounting.json').subscribe(data => { products = data; });
    } catch (error) { console.log(error); return null; }
    try {
      return this.http.get(build.buildURI + '/assets/cities/altayskiy_kray/filters_ac.json').pipe(
        switchMap(json => {
          let formData: FormData = new FormData;
          formData.append('buttons', JSON.stringify(json));
          formData.append('products', JSON.stringify(products));
          try {
            return this.http.post(build.buildAPI, formData);
          } catch (error) { console.log(error); return null; }
        })
      );
    } catch (error) { console.log(error); return null; }
  }

  constructor(private http:HttpClient) { }
}
