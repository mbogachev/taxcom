import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Company } from '../Classes/company';
import {build} from '../Classes/build';


@Injectable({
    providedIn: 'root'
})
export class CompanyGetterService {
    GetCompany() {
        return this.http.get<Company>(build.buildURI+'/assets/companies/companies.json');
    }
    getCompany(){
        let formData = new FormData();
        formData.append('companies', JSON.stringify('notNull'));
        return this.http.post(build.buildAPI, formData);
    }

    constructor(private http: HttpClient) { }
}