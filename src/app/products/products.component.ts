import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductGetterService } from '../Services/product-getter.service';

@Component({
  selector: 'vex-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  INN;
  typeCompany;
  servicesType:string;
  productsList=null;
  classList;

  onChange(increased){
    this.productsList=increased;
  }
  goBack(){
    window.location.replace(window.location.origin+'/apps/taxcom/');
  }
  openLink(link){
    if(this.typeCompany!=null&&this.typeCompany.name!="null"){
      window.open(link + "&inn=" + this.INN);
    }
    else{
      window.open(link);
    }
  }

  constructor(private http: HttpClient,private resolver: ComponentFactoryResolver, private route: ActivatedRoute, private productGetter: ProductGetterService) {
    this.route.queryParams.subscribe(params => {
      let service = params['page'];
      if (service == 'accounting') {
        this.servicesType = "Отчётность";
        this.INN = params['INN'];
        if (this.INN == "null" || this.INN == undefined || this.INN == null) {
          this.typeCompany = {name:"null"};
        } else if (this.INN.length == 10) {
          this.typeCompany = {name:"ООО", button:"L1_B2"};
        } else if (this.INN.length == 12) {
          this.typeCompany = {name:"ИП",button:"L1_B1"};
        }
        else {
          this.typeCompany = { name: "null" };
        }
      }
      else if (service == 'electronic_signatures') {
        this.servicesType = "Электронная подпись";
      }
    });
   }

  ngOnInit(): void {
  }

}
