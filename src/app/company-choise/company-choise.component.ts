import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Company } from '../Classes/company';
import { CompanyGetterService } from '../Services/company-getter.service';

@Component({
  selector: 'vex-company-choise',
  templateUrl: './company-choise.component.html',
  styleUrls: ['./company-choise.component.scss']
})
export class CompanyChoiseComponent implements OnInit {

  companies;
  selectedCompany
  @Output() onChange = new EventEmitter<Company>() ;

  selectCompany(company){
    this.onChange.emit(company);
  }

  constructor(private companyGetter: CompanyGetterService) {
    this.companyGetter.getCompany().subscribe(company =>this.companies=company);
   }

  ngOnInit(): void {
  }

}
