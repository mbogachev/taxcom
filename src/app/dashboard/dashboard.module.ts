import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { CompanyChoiseComponent } from '../company-choise/company-choise.component';



@NgModule({
  declarations: [DashboardComponent, CompanyChoiseComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FlexLayoutModule,
    MatButtonModule
  ]
})
export class DashboardModule { }
