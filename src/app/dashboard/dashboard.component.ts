import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Company } from '../Classes/company';
import { ClassesGetterService } from '../Services/classes-getter.service';
import { TasksGetter } from '../Services/task-getter.service';

@Component({
  selector: 'vex-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  selectedCompany;
  classList;
  onChange(increased:Company){
    this.selectedCompany = increased;
  }
  gotoProduct(link, company){
    if(company==undefined||company==null){
      this.router.navigate(['/products'], { queryParams: { 'page': link, 'INN': "null" } });
    }else{
      this.router.navigate(['/products'], { queryParams: { 'page': link, 'INN': company.INN } });
    }
  }
  createTask(){
    let task;
    this.taskGetter.setTask().subscribe(data=>{task = data;});
    console.log(task);
    if ((task != null || task != undefined) && task !='Такая задача уже создана'){
      let myElement = document.createElement('div');
      myElement.innerHTML = '<p style="color:red;">У вас уже есть созданная задача'+task.dateTime+'</p>';
      document.querySelector('.createTask').appendChild(myElement);
    }
    else{
      let myElement = document.createElement('div');
      myElement.innerHTML = '<p style="color:green;text-align:center;">Задача успешно создана</p><br/>';
      document.querySelector('.createTask').appendChild(myElement);
      document.querySelector('.taskButton').innerHTML = "";
    }
  }

  constructor(private classesGetter:ClassesGetterService, private router:Router, private taskGetter:TasksGetter) {
    classesGetter.GetClasses().subscribe(
      (data) => {
        this.classList=data;
        this.classList.forEach(element => {
          let length;
          length = element.file_name.length;
          element.link = element.file_name.substr(0, length-5);
          return element;
        });
      }
    );
  }

  ngOnInit(): void {
  }

}
