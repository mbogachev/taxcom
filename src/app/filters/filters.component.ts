import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FiltersGetterService } from '../Services/filters-getter.service';
import { ProductGetterService } from '../Services/product-getter.service';

@Component({
  selector: 'vex-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Input() typeCompany;
  clickedL1: any = 0;
  clickedL2: any = 0;
  clickedL3: any = 0;
  serviceType;
  typeElSignatures;
  nameAcBtns={L1_B1:'ИП', L1_B2:'ЮЛ',L1_B3:'Группа компаний', L1_B4:'Бюджетное учреждение',L1_B5:'Налоговый представитель',
    L2_B1:'Онлайн', L2_B2:'Локальный комплекс',L2_B3:'Решение, встроенное в 1С',
    L3_B1: 'УСН, ПСН, ЕСН', L3_B2: 'ОСНО',L3_B3: 'Нулевая отчётность'};
  selectedTypeES;
  typeAccounting;
  products;
  displayedProducts = [];
  @Output() onChange = new EventEmitter();

  clickL1(key){
    if(this.clickedL1!=key){
      for (const [k, value] of Object.entries(this.typeAccounting.L1)) {
        try{
          if (document.querySelector('.' + k + '>div>div>button')!=null &&
            document.querySelector('.' + k + '>div>div>button').getAttribute("class") == "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary"
            && key != k) {
            document.querySelector('.' + k + '>div>div>button').setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
          }
        }catch(error){console.log(error);}
      }
      this.clickedL1=key;
      this.clickedL2=0;
      this.clickedL3=0;
      this.displayedProducts=[];
      this.products.forEach(element=>{
        if(element.filters.L1==key){
          this.displayedProducts.push(element);
        }
      });
      if(this.checkTypeCompany(false)==false){
        let domElement = document.querySelector('.' + key + '>div>div>button');
        domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary");
      }
      
    }else{
      if (this.checkTypeCompany(false) == false) {
        try {
          let domElement = document.querySelector('.' + key + '>div>div>button');
          domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
        } catch (error) { console.log(error); }
      }
      this.clickedL1 = 0;
      this.clickedL2 = 0;
      this.clickedL3 = 0;
      this.displayedProducts=[];
    }
    this.onChange.emit(this.displayedProducts);
  }
  clickL2(key){
    let domElement = document.querySelector('.' + key + '>div>div>button');
    if(this.clickedL2!=key){
      try {
        for (const [k, value] of Object.entries(this.typeAccounting.L1[this.clickedL1].L2)) {
          if (document.querySelector('.' + k + '>div>div>button') != null && 
            document.querySelector('.' + k + '>div>div>button').getAttribute("class") == "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary"
            && key != k) {
            document.querySelector('.' + k + '>div>div>button').setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
          }
        }
      } catch (error) { console.log(error); }
      this.clickedL2=key;
      this.clickedL3=0;
      this.displayedProducts = [];
      this.products.forEach(element=>{
        if(element.filters.L1==this.clickedL1&&element.filters.L2==key){
          this.displayedProducts.push(element);
        }
      });
      domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary");
    }else{
      try {
        domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
      } catch (error) { console.log(error); }
      let k = this.clickedL1;
      this.clickedL1=0;
      this.clickedL2=0;
      this.clickedL3=0;
      if (this.checkTypeCompany(false) == false) {
        this.clickL1(k);
      }
    }
    this.onChange.emit(this.displayedProducts);
  }
  clickL3(key){
    let domElement = document.querySelector('.' + key + '>div>div>button');
    if(this.clickedL3!=key){
      for(const [k,value] of Object.entries(this.typeAccounting.L1[this.clickedL1].L2[this.clickedL2].L3)){
        if (document.querySelector('.' + k + '>div>div>button').getAttribute("class") =="mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary"
        &&key!=k){
          document.querySelector('.' + k + '>div>div>button').setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
        }
      }
      this.clickedL3=key;
      this.displayedProducts=[];
      this.products.forEach(element=>{
        if(element.filters.L1==this.clickedL1&&element.filters.L2==this.clickedL2&&element.filters.L3==key){
          this.displayedProducts.push(element);
        }
      });
      domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base mat-primary");
    }else{
      domElement.setAttribute("class", "mat-focus-indicator rounded-half max-w-full w-200 mat-raised-button mat-button-base");
      let k1 = this.clickedL1;
      let k2 = this.clickedL2;
      this.clickedL1 = 0;
      this.clickedL2 = 0;
      this.clickedL3 = 0;
      if (this.checkTypeCompany(false)==false){
        this.clickL1(k1);
      }
      this.clickL2(k2);
    }
    this.onChange.emit(this.displayedProducts);
  }

  checkTypeCompany(onConstructor){
    if(onConstructor==true){
      if (this.typeCompany.name == "ИП") {
        try { this.clickL1("L1_B1"); } catch (error) { console.log(error); }
      }
      else if (this.typeCompany.name == "ООО") {
        try { this.clickL1("L1_B2"); } catch (error) { console.log(error); }
      }
      else {
        try { document.querySelector('.L1_B3').remove(); } catch (error) { console.log(error); }
        try { document.querySelector('.L1_B5').remove(); } catch (error) { console.log(error); }
        try { this.clickedL1 = 0; } catch (error) { console.log(error); }
        return false;
      }
    }else{
      if(this.typeCompany.name=="ИП"){
        return this.typeCompany.name;
      } else if(this.typeCompany.name=="ООО"){
        return this.typeCompany.name;
      }else{
        return false;
      }
    }
  }

  filterES(key){
    if(this.displayedProducts!=null){
      this.displayedProducts = [];
      this.typeElSignatures.forEach(element=>{
        document.querySelector('.filterES > li.' + element.id + ' > h3 > hr.my-3').
        setAttribute('style', 'border-top: 2px dashed rgb(2, 2, 2); width: 100%; height: 1px;');
      });
    }
    this.typeElSignatures.forEach(element=>{
      if(element.id==key){
        this.products.forEach(product=>{
          if(product.filter==element.name){
            this.displayedProducts.push(product);
          }
        });
        try{
          document.querySelector('.filterES > li.' + key + ' > h3 > hr.my-3').
          setAttribute('style', 'border-top: 2px solid rgb(59, 130, 246); width: 100%; height: 1px;')
        }catch(e){console.log(e);}
      }
    });
    this.onChange.emit(this.displayedProducts);
  }

  constructor(private route: ActivatedRoute, private filterGetter: FiltersGetterService, private productGetter: ProductGetterService) {
    this.route.queryParams.subscribe(params => {
      let service = params['page'];
      if(service=="accounting"){
        this.serviceType="Отчётность";
        try{
          filterGetter.getAFilter().subscribe(data => { this.typeAccounting = data; });
        }catch(error){console.log(error);}
        try{
          productGetter.getAccounting().subscribe(data => {
            this.products = data;
            this.checkTypeCompany(true);
          });
        } catch (error) { console.log(error);}
      }
      if(service=="electronic_signatures"){
        this.serviceType ="Электронная подпись";
        try{
          productGetter.getElectronicSignatures().subscribe(data=>{
            this.products = data;
          });
        } catch (error) { console.log(error); }
        try {
          filterGetter.getESFilter().subscribe(data => {this.typeElSignatures = data;});
         } catch (error) { console.log(error); }
      }
    });
   }

  ngOnInit(): void {
  }
}
