<?php
if(isset($_POST['buttons'])&&isset($_POST['products'])){
    $newJson;
    $newButtons;
    $json = json_decode($_POST['products'], JSON_OBJECT_AS_ARRAY);
    $buttons = json_decode($_POST['buttons'], JSON_OBJECT_AS_ARRAY);
    $buttonsNames = array(
        'L1_B1' => 'ИП', 'L1_B2' => 'ЮЛ', 'L1_B3' => 'Группа компаний', 'L1_B4' => 'Бюджетное учреждение', 'L1_B5' => 'Налоговый представитель',
        'L2_B1' => 'Онлайн', 'L2_B2' => 'Локальный комплекс', 'L2_B3' => 'Решение, встроенное в 1С',
        'L3_B1' => 'УСН, ПСН, ЕСН', 'L3_B2' => 'ОСНО', 'L3_B3' => 'Нулевая отчётность'
    ); //Массив имён и ключей кнопок
    $i = 0; //Вспомогательная переменная для ключей продуктов
    foreach($json as $key=>$value){
        if(is_array($value["links"])&&!empty($value["links"])){
            foreach ($buttons['L1'] as $kL1 => $bL1) {
                foreach ($buttonsNames as $kNames => $names) {
                    if ($kL1 == $kNames) {
                        if (is_array($bL1['L2']) && !empty($bL1['L2'])) {
                            foreach ($bL1['L2'] as $kL2 => $bL2) {
                                foreach ($buttonsNames as $kL2Names => $L2names) {
                                    if ($kL2 == $kL2Names) {
                                        if (isset($bL2['displayCard']) && !$bL2['L3'] && in_array($key, $bL2['displayCard'])) {
                                            if (is_array($value["files"]) && !empty($value["files"])) {
                                                $newJson[$i] = array(
                                                    "id" => $key, "name" => $value["name"], "prices" => $value["prices"], "links" => $value["links"],
                                                    "files" => "https://taxcom.ru" . $value["files"]["specification"], 
                                                    "filters" => array("L1" => $kL1, "L2" => $kL2)
                                                );
                                            } else {
                                                $newJson[$i] = array(
                                                    "id" => $key, "prices" => $element["prices"], "links" => $element["links"],
                                                    "filters" => array("L1" => $kL1, "L2" => $kL2)
                                                );
                                            }
                                            $i++;
                                        }
                                        if (is_array($bL2['L3']) && !empty($bL2['L3'])) {
                                            foreach ($bL2['L3'] as $kL3 => $bL3) {
                                                foreach ($buttonsNames as $kL3Names => $L3names) {
                                                    if (is_array($bL3['displayCard']) && in_array($key, $bL3['displayCard'])) {
                                                        if ($kL3 == $kL3Names) {
                                                            if (is_array($value["files"]) && !empty($value["files"])) {
                                                                $newJson[$i] = array(
                                                                    "id" => $key, "name" => $value["name"], "prices" => $value["prices"], "links" => $value["links"],
                                                                    "files" => "https://taxcom.ru" . $value["files"]["specification"],
                                                                    "filters" => array("L1" => $kL1, "L2" => $kL2, "L3" => $kL3)
                                                                );
                                                            } else {
                                                                $newJson[$i] = array(
                                                                    "id" => $key, "prices" => $element["prices"], "links" => $element["links"],
                                                                    "filters" => array("L1" => $kL1, "L2" => $kL2, "L3" => $kL3)
                                                                );
                                                            }
                                                            $i++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // file_put_contents('supra.json', json_encode($newJson, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
    echo json_encode($newJson);
    return json_encode($newJson);
}
if (isset($_POST['task'])) {
    if (file_exists(__DIR__ . '/tasks/task.json')) {
        $task = json_decode(file_get_contents(__DIR__.'/tasks/task.json'), JSON_OBJECT_AS_ARRAY);
        if(json_decode($_POST['task'], JSON_OBJECT_AS_ARRAY)==end($task)){
            echo 'Такая задача уже создана';
            return 'Такая задача уже создана';
        }else{
            file_put_contents(__DIR__.'/tasks/task.json',$_POST['task'].',', FILE_APPEND);
            //echo json_encode(end($task), JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            echo 'Задача успешно создана';
            return 'Задача успешно создана';
        }
    }else{
        mkdir(__DIR__.'/tasks/', 0777);
        file_put_contents(__DIR__.'/tasks/task.json',$_POST['task'].',', FILE_APPEND);
        echo 'Задача успешно создана';
        return 'Задача успешно создана';
    }
}
if (isset($_POST['companies'])) {
    if (file_exists(__DIR__ . '/companies/companies.json')) {
        $companies = json_decode(file_get_contents(__DIR__ . '/companies/companies.json'), JSON_OBJECT_AS_ARRAY);
        if($companies!=null){
            echo json_encode($companies, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            return json_encode($companies, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }else{
            echo 'Ошибка получения списка компаний';
            return 'Ошибка получения списка компаний';
        }
    }else{
        echo 'Ошибка получения списка компаний';
        return 'Ошибка получения списка компаний';
    }
}
?>